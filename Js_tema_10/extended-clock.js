class ExtendedClock extends Clock {
    start(ms=1000) {
      this.render();
      this.timer = setInterval(() => this.render(),ms);
    }
  };