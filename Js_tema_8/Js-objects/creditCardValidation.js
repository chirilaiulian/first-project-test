console.log('Credit Card Validation (ex4)');
console.log('          V         ');


function validateCreditCard(CC) {
    //length check
    if (CC.length !== 16) {
        return false;
    }

    //all digits must be numbers
    for (let i = 0; i < CC.length; i++) {
        if (CC.match(/^[0-9]+$/) === null) {
            return false
        }
    }

    //final digit must be even
    if (CC.charAt(CC.length - 1) % 2 !== 0) {
        return false;
    }

    //sum of all digits must be greater than 16
    let sum = 0;
    for (let i = 0; i < CC.length; i++) {
        sum += Number(CC[i]);
    }
    if (sum < 16){
        return false;
    }

    //default value
    return true;
};

console.log(validateCreditCard('9999777788880000'));//valid
console.log(validateCreditCard('6666666666661666'));//valid
console.log(validateCreditCard('a92332119c011112'));//invalid
console.log(validateCreditCard('4444444444444444'));//invalid
console.log(validateCreditCard('111111111111110'));//invalid
console.log(validateCreditCard('6666666666666661'));//invalid