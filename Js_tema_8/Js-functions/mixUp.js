console.log('Mix Up (ex8)');
console.log('  V         ');


function mixUp(string1, string2) {
    let mixedString = string2.slice(0, 2) + string1.slice(2) + " " + string1.slice(0, 2) + string2.slice(2);
    return mixedString;
}

console.log(mixUp('mix', 'pod'));
console.log(mixUp('dog', 'dinner'));

console.log('');