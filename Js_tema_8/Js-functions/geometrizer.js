console.log('The Geometrizer (ex4)');
console.log('       V         ');


function calcCircumference(radius) {
    let circumference = Math.round(2 * Math.PI * radius);
    console.log(`The circumference is ${circumference}`);
}

calcCircumference(20);

console.log('');