//selecting all required elements
const selectBox = document.querySelector(".select-box"),
    selectBtnX = selectBox.querySelector(".options .playerX"),
    selectBtnO = selectBox.querySelector(".options .playerO"),
    playBoard = document.querySelector(".play-board"),
    players = document.querySelector(".players"),
    allBox = document.querySelectorAll("section span"),
    resultBox = document.querySelector(".result-box"),
    wonText = resultBox.querySelector(".won-text"),
    replayBtn = resultBox.querySelector("button");
restartBtn = document.getElementById("restart");

window.onload = () => { //once window loaded
    for (let i = 0; i < allBox.length; i++) { //add onclick attribute in all available span elements
        allBox[i].setAttribute("onclick", "clickedBox(this)");
        allBox[i].style.cursor = 'pointer';
    }
}

selectBtnX.onclick = () => {
    selectBox.classList.add("hide"); //hide select box
    playBoard.classList.add("show"); //show the playboard section
}

selectBtnO.onclick = () => {
    selectBox.classList.add("hide"); //hide select box
    playBoard.classList.add("show"); //show the playboard section
    players.setAttribute("class", "players active player"); //set class attribute in players with players active player values
    players.classList.remove("active");
    let randomTimeDelay = ((Math.random() * 1000) + 200).toFixed(); //generating random number so the bot will randomly delay to select empty box
    setTimeout(() => {
        bot(runBot); //calling bot function
    }, randomTimeDelay); //passing random delay value
}


let playerXIcon = "fa-solid fa-xmark"; //class name of fontawesome cross icon
let playerOIcon = "fa-solid fa-o"; //class name of fontawesome circle icon
let playerSign = "X"; //this is a global variable because we've used this variable inside multiple functions
let runBot = true; //this also a global variable with boolen value..we used this variable to stop the bot once the match is won by someone or drawn
let counter = 0; //global variable to check number of turns

// user click function
function clickedBox(element) {
    if (players.classList.contains("player")) {
        playerSign = "O"; //if player choose (O) then change playerSign to O
        element.innerHTML = `<i class="${playerOIcon} player-signs-centering"></i>`; //adding circle icon tag inside user clicked element/box
        players.classList.remove("active"); //add active class in players
        element.setAttribute("id", playerSign); //set id attribute in span/box with player choosen sign
        counter++;
    } else {
        element.innerHTML = `<i class="${playerXIcon} player-signs-centering"></i>`; //adding cross icon tag inside user clicked element/box
        players.classList.add("active"); //add active class in players
        element.setAttribute("id", playerSign); //set id attribute in span/box with player choosen sign
        counter++;
    }
    selectWinner(); //caling selectWinner function
    element.style.pointerEvents = "none"; //once user selects any box then that box can'be clicked again
    playBoard.style.pointerEvents = "none"; //add pointerEvents none to playboard so user can't immediately click on any other box until bot selects
    let randomTimeDelay = ((Math.random() * 1000) + 200).toFixed(); //generating random number so bot will randomly delay to select unselected box
    setTimeout(() => {
        bot(runBot); //calling bot function
    }, randomTimeDelay); //passing random delay value
}

// bot auto select function
function bot() {
    let array = []; //creating empty array...we'll store unclicked boxes index
    if (runBot) { //if runBot is true
        playerSign = "O"; //change the playerSign to O so if player has chosen X then bot will O
        for (let i = 0; i < allBox.length; i++) {
            if (allBox[i].childElementCount == 0) { //if the box/span has no children means <i> tag
                array.push(i); //inserting unclicked boxes number/index inside array
            }
        }
        let randomBox = array[Math.floor(Math.random() * array.length)]; //getting random index from array so bot will select random unselected box
        if (array.length > 0) { //if array length is greater than 0
            if (players.classList.contains("player")) {
                playerSign = "X"; //if player has chosen O then bot will X
                allBox[randomBox].innerHTML = `<i class="${playerXIcon} player-signs-centering"></i>`; //adding cross icon tag inside bot selected element
                players.classList.add("active"); //remove active class in players
                allBox[randomBox].setAttribute("id", playerSign); //set id attribute in span/box with player choosen sign
                counter++;
            } else {
                allBox[randomBox].innerHTML = `<i class="${playerOIcon} player-signs-centering"></i>`; //adding circle icon tag inside bot selected element
                players.classList.remove("active"); //remove active class in players
                allBox[randomBox].setAttribute("id", playerSign); //set id attribute in span/box with player choosen sign
                counter++;
            }
            selectWinner(); //calling selectWinner function
        }
        allBox[randomBox].style.pointerEvents = "none"; //once bot select any box then user can't click on that box
        playBoard.style.pointerEvents = "auto"; //add pointerEvents auto in playboard so user can again click on box
        playerSign = "X"; //needed to debug situations where X would win but O would be credited for it :)
    }
}

function getIdVal(classname) {
    return document.querySelector(".box" + classname).id; //return id value
}
function checkIdSign(val1, val2, val3, sign) { //checking all id value is equal to sign (X or O) or not, if yes then return true
    if (getIdVal(val1) == sign && getIdVal(val2) == sign && getIdVal(val3) == sign) {
        return true;
    }
}
function selectWinner() { //if the one of following winning combination match then select the winner
    if (checkIdSign(1, 2, 3, playerSign) || checkIdSign(4, 5, 6, playerSign) || checkIdSign(7, 8, 9, playerSign) || checkIdSign(1, 4, 7, playerSign) || checkIdSign(2, 5, 8, playerSign) || checkIdSign(3, 6, 9, playerSign) || checkIdSign(1, 5, 9, playerSign) || checkIdSign(3, 5, 7, playerSign)) {
        runBot = false; //passing the false boolen value to runBot so bot won't run again
        bot(runBot); //calling bot function
        setTimeout(() => { //after match won by someone then hide the playboard and show the result box after 1500ms
            resultBox.classList.add("show");
            playBoard.classList.remove("show");
        }, 1500); //1s = 1000ms
        wonText.innerHTML = `<p>${playerSign}</p> won the game!`; //displaying winning text with passing playerSign (X or O)
    } else { //if all boxes/element have id value and still no one win then draw the match
        if (getIdVal(1) != "" && getIdVal(2) != "" && getIdVal(3) != "" && getIdVal(4) != "" && getIdVal(5) != "" && getIdVal(6) != "" && getIdVal(7) != "" && getIdVal(8) != "" && getIdVal(9) != "" && counter === 9) {
            runBot = false; //passing the false boolen value to runBot so bot won't run again
            bot(runBot); //calling bot function
            setTimeout(() => { //after match drawn then hide the playboard and show the result box after 1500ms
                resultBox.classList.add("show");
                playBoard.classList.remove("show");
            }, 1500); //1s = 1000ms
            wonText.textContent = "Draw!"; //displaying draw match text
        }
    }
}

function resetTable() {
    for (let i = 0; i < allBox.length; i++) {
        allBox[i].setAttribute("id", null);
        allBox[i].innerHTML = "";
        allBox[i].style.pointerEvents = "auto";
        counter = 0;
    }
}

restartBtn.addEventListener('click', resetTable);

replayBtn.onclick = () => {
    window.location.reload(); //reload the current page on replay button click
}

