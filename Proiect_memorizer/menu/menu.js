let mainGame = document.getElementsByClassName('game')[0];
let mainMenu = document.getElementsByClassName('main-menu')[0];
let boardSizeMenu = document.getElementsByClassName('board-size-menu')[0];
let playerNamesMenu = document.getElementsByClassName('player-names-menu')[0];

let singleBtn = document.getElementById('single'); //main menu btns
let multiBtn = document.getElementById('multi'); //main menu btns

let playSingle = document.getElementById('single-play');//game mode btns
let playMulti = document.getElementById('multi-play');//game mode btns

let nextBtn = document.getElementById('next');

let restartGame = document.getElementsByClassName('restart-game')[0];
let radio3x4 = document.getElementById('radio3x4');
let radio4x4 = document.getElementById('radio4x4');

let player1InputName = document.getElementById('player1-name');
let player2InputName = document.getElementById('player2-name');
let player1Container = document.getElementsByClassName('player1-stats')[0];
let player2Container = document.getElementsByClassName('player2-stats')[0];

let backToMenu = document.getElementsByClassName('back-to-menu')[0];


//event listener for the main menu singleplayer btn
singleBtn.addEventListener('click', () => {
    mainMenu.classList.remove('visible');
    mainMenu.classList.add('hidden');

    boardSizeMenu.classList.remove('hidden');
    boardSizeMenu.classList.add('visible');

    nextBtn.classList.add('gone');

    playSingle.classList.remove('gone');
})

//button to trigger sinple player game with currently selected settings
playSingle.addEventListener('click', () => {
    mainGame.classList.remove('hidden');
    mainGame.classList.add('visible');

    mainMenu.classList.add('hidden');
    mainMenu.classList.remove('visible');

    boardSizeMenu.classList.add('hidden');
    boardSizeMenu.classList.remove('visible');

    player1Container.classList.remove('visible');
    player1Container.classList.add('hidden');

    player2Container.classList.remove('visible');
    player2Container.classList.add('hidden');

    selectors.singlePlayerScore.innerHTML = "Score:";
    selectors.score.innerHTML = "0";
    multiplayer = false;
    restartGameFunc(); //restarts the game (if removed the game state gets saved)
})

nextBtn.addEventListener('click', () => {
    boardSizeMenu.classList.add('hidden');
    boardSizeMenu.classList.remove('visible');

    playerNamesMenu.classList.add('visible');
    playerNamesMenu.classList.remove('hidden');
})

//multiplayer btn function
function multi() {

    if (player1InputName.value != "" && player2InputName.value != "") {

        mainGame.classList.remove('hidden');
        mainGame.classList.add('visible');

        mainMenu.classList.add('hidden');
        mainMenu.classList.remove('visible');

        player1Container.classList.add('visible');
        player1Container.classList.remove('hidden');

        player2Container.classList.add('visible');
        player2Container.classList.remove('hidden');

        playerNamesMenu.classList.remove('visible');
        playerNamesMenu.classList.add('hidden');

        selectors.singlePlayerScore.innerHTML = "";
        selectors.score.innerHTML = "";
        multiplayer = true;
        restartGameFunc();
    } else {
        player1InputName.placeholder = "Missing name";
        player2InputName.placeholder = "Missing name";

        if (player1InputName.value === "" && player2InputName.value === "") {
            player1InputName.classList.add('error');
            player2InputName.classList.add('error');
        }
        if (player1InputName.value === "") {
            player1InputName.classList.add('error');
        }
        if (player2InputName.value === "") {
            player2InputName.classList.add('error');
        }
    }
}

player1InputName.addEventListener('keydown', () => {
    player1InputName.classList.remove('error');
    player1InputName.placeholder = "Player 1";
})

player2InputName.addEventListener('keydown', () => {
    player2InputName.classList.remove('error');
    player2InputName.placeholder = "Player 2";
})

//game options play event listener
playMulti.addEventListener('click', multi);

//main menu multiplayer btn event listener
multiBtn.addEventListener('click', () => {

    mainMenu.classList.remove('visible');
    mainMenu.classList.add('hidden');

    boardSizeMenu.classList.remove('hidden');
    boardSizeMenu.classList.add('visible');

    playSingle.classList.add('gone');

    nextBtn.classList.remove('gone');
});

//preventing white spaces in names
player1InputName.addEventListener('keypress', (e) => {
    let key = e.keyCode;
    if (key === 32) {
        e.preventDefault();
    }
    if (key === 13) {
        multi();
    }
})

player2InputName.addEventListener('keypress', (e) => {
    let key = e.keyCode;
    if (key === 32) {
        e.preventDefault();
    }
    if (key === 13) {
        multi();
    }
})


//event listener for the back to menu btn
backToMenu.addEventListener('click', () => {

    mainGame.classList.remove('visible');
    mainGame.classList.add('hidden');

    mainMenu.classList.remove('hidden');
    mainMenu.classList.add('visible');

    nextBtn.classList.add('gone');

    selectors.controls.classList.remove('hidden');
})

radio3x4.addEventListener('click', () => {
    if (radio3x4.checked === true) {
        rows = 3;
    }
})

radio4x4.addEventListener('click', () => {
    if (radio4x4.checked === true) {
        rows = 4;
    }
})

//event listener for the restart game btn
restartGame.addEventListener('click', () => {
    restartGameFunc()
})