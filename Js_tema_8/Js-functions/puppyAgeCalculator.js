console.log('The Puppy Age Calculator (ex2)');
console.log('          V         ');


function calculateDogAge(dogAge, humanAge) {
    let doggie = Math.round(dogAge * 7);
    let human = Math.round(humanAge / 7);
    console.log(`Your doggie is ${doggie} years old in human years!`);
    console.log(`Your human is ${human} years old in dog years!`);
}

calculateDogAge(20, 60);
calculateDogAge(5, 40);
calculateDogAge(12, 30);

console.log('');