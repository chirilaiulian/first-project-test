console.log('The Movie Database (ex3)');
console.log('        V         ');


let movies =
{
    title: 'Puff the Magic Dragon',
    duration: 30,
    stars: ['Puff', 'Jackie', 'Living Sneezes']
};


function printMovieInfo(object) {
    console.log(`${object.title} lasts for ${object.duration} minutes.`);
    console.log(`Stars: ${object.stars}.`);
}

printMovieInfo(movies);

console.log('');