const pi = 3.141592;

let circleRadius = 25;
let circleCircumference = 2 * pi * circleRadius;
let circleArea = pi * circleRadius ** 2;

alert(`The circle circumference is ${circleCircumference}`);
alert(`The area is ${circleArea}`);