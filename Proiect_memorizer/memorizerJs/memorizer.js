let selectors = {

    controls: document.querySelector('.controls'),
    bottomMenu: document.querySelector('.bottom-menu-btn'),

    boardContainer: document.querySelector('.board-container'),
    board: document.querySelector('.board'),
    moves: document.querySelector('.moves'),
    score: document.querySelector('.score'),
    timer: document.querySelector('.timer'),

    player1Name: document.querySelector('.player1-display'),
    player1Moves: document.querySelector('.player1-moves'),
    player1Score: document.querySelector('.player1-score'),

    player2Name: document.querySelector('.player2-display'),
    player2Moves: document.querySelector('.player2-moves'),
    player2Score: document.querySelector('.player2-score'),

    singlePlayerScore: document.getElementById('single-player-score'),

    win: document.querySelector('.win'),
}

let state = {
    gameStarted: false,
    flippedCards: 0,
    totalFlips: 0,
    totalTime: 0,
    score: 0,
    loop: null
}

//default table size
let rows = 3;
let cols = 4;

// variables used for moves and scoring system later
let multiplayer = false;

let p1Moves = 0;
let p1Score = 0;

let p2Moves = 0;
let p2Score = 0;

let player1Turn = true;

//function to generate the table cards + pick random pairs of emojis
let generateGame = () => {

    selectors.boardContainer.innerHTML = "";

    const emojis = ['🍐', '🍎', '🫐', '🌽', '🥕', '🍇', '🍉', '🍌', '🥭', '🍍'];
    let picks = 0;

    //if table dimensions are odd, add 1 more pair of cells
    if (rows % 2 === 0) {
        picks = pickRandom(emojis, (rows * rows) / 2);
    } else {
        picks = pickRandom(emojis, (rows * rows) / 2 + 1);
    }


    let items = shuffle([...picks, ...picks])
    let cards = `
        <div class="board" style="grid-template-columns: repeat(${cols}, auto)">
            ${items.map(item => `
                <div class="card">
                    <div class="card-front"></div>
                    <div class="card-back">${item}</div>
                </div>
            `).join('')}
       </div>
    `
    //adding the cards object (containing all the board cells) to the game board container
    selectors.boardContainer.innerHTML = cards;
}

//function to randomize the position of each cell
let pickRandom = (array, items) => {
    let clonedArray = [...array];
    let randomPicks = [];

    for (let index = 0; index < items; index++) {
        let randomIndex = Math.floor(Math.random() * clonedArray.length);

        randomPicks.push(clonedArray[randomIndex]);
        clonedArray.splice(randomIndex, 1);
    }

    return randomPicks;
}

let shuffle = array => {
    let clonedArray = [...array];

    for (let index = clonedArray.length - 1; index > 0; index--) {
        let randomIndex = Math.floor(Math.random() * (index + 1));
        let original = clonedArray[index];

        clonedArray[index] = clonedArray[randomIndex];
        clonedArray[randomIndex] = original;
    }

    return clonedArray;
}

//add event listeners on all buttons or cells in the game
let attachEventListeners = () => {
    document.addEventListener('click', event => {
        let eventTarget = event.target;
        let eventParent = eventTarget.parentElement;
        if (eventTarget.className.includes('card') && !eventParent.className.includes('flipped')) {
            flipCard(eventParent);
        } else if (eventTarget.nodeName === 'BUTTON' && !eventTarget.className.includes('disabled')) {
            startGame();
        }
    })
}

//start game function
let startGame = () => {

    state.gameStarted = true;

    state.loop = setInterval(() => {
        state.totalTime++;

        // scoring system for singleplayer
        selectors.moves.innerText = `${state.totalFlips}`;
        if (!multiplayer) {
            selectors.score.innerHTML = `${state.score}`;
        }
        selectors.timer.innerText = `${state.totalTime}s`;
    }, 1000)

    if (multiplayer) {
        //refreshing the moves and score board much faster (previously it was lagging behind too much)
        setInterval(() => {
            selectors.player1Moves.innerHTML = `${p1Moves}`;
            selectors.player2Moves.innerHTML = `${p2Moves}`;

            selectors.player1Score.innerHTML = `${p1Score}`;
            selectors.player2Score.innerHTML = `${p2Score}`;
        }, 100)
    }

}

let flipCard = card => {

    //counters for number of fliped cards and total flips (singleplayer)
    state.flippedCards++;

    if (!state.gameStarted) {
        startGame();
    }

    if (state.flippedCards <= 2) {

        if (multiplayer) {
            //counter for multiplayer moves depending on who's turn it is
            if (player1Turn) {
                p1Moves++;
            } else {
                p2Moves++;
            }

        }

        state.totalFlips++;
        card.classList.add('flipped');
        //make already visible cards not clickable (pointer events: none;)
        card.classList.add('not-clickable');
    }

    if (state.flippedCards === 2) {
        let flippedCards = document.querySelectorAll('.flipped:not(.matched)')
        document.removeEventListener('click', event => {
            let eventTarget = event.target;
            let eventParent = eventTarget.parentElement;
            if (eventTarget.className.includes('card') && !eventParent.className.includes('flipped')) {
                flipCard(eventParent);
            } else if (eventTarget.nodeName === 'BUTTON' && !eventTarget.className.includes('disabled')) {
                startGame();
            }
        })

        if (flippedCards[0].innerText === flippedCards[1].innerText) {
            flippedCards[0].classList.add('matched');
            flippedCards[1].classList.add('matched');

            //scoring counter for singleplayer
            state.score++;

            //scoring system for  multiplayer depending on who's turn it is
            if (player1Turn) {
                p1Score++;

            } else {
                p2Score++;
            }

        }
        if (multiplayer) {
            player1Turn = !player1Turn; //switching player turns
            //function to add a class to the current players turn
            currentTurn(player1Turn)
        }

        //flip back the cards after 1 second
        setTimeout(() => {
            flipBackCards();
        }, 1000)
    }
}

//function to flip back the cards
let flipBackCards = () => {
    document.querySelectorAll('.card:not(.matched)').forEach(card => {
        //classes to make the card hidden and clickable again once fliped back to original state
        card.classList.remove('flipped');
        card.classList.remove('not-clickable');
    })

    state.flippedCards = 0;

    if (!document.querySelectorAll('.card:not(.flipped)').length) {
        setTimeout(() => {

            //hide game stats
            selectors.controls.classList.add('hidden');
            selectors.boardContainer.classList.add('flipped');

            //determining the winner
            let winner, moves, score;
            if (p1Score > p2Score) {
                winner = player1InputName.value;
                moves = p1Moves;
                score = p1Score;
            } else {
                winner = player2InputName.value;
                moves = p2Moves;
                score = p2Score;
            }

            //display win / draw messages according to singleplayer / multiplayer cases
            if (!multiplayer) {
                selectors.boardContainer.innerHTML = `
            <div class="win">
                <span class="win-text">
                    You won!<br />
                    with <span class="highlight">${state.totalFlips}</span> moves<br />
                    in <span class="highlight">${state.totalTime}</span> seconds
                </span>
            </div>
            `
            } else {
                if (p1Score === p2Score) {
                    selectors.boardContainer.innerHTML = `
            <div class="win">
                <span class="win-text">
                    Draw!<br />
                </span>
            </div>
            `
                } else {
                    selectors.boardContainer.innerHTML = `
            <div class="win">
                <span class="win-text">
                    ${winner} won!<br />
                    with a score of <span class="highlight">${score}</span><br />
                    with <span class="highlight">${moves}</span> moves
                </span>
            </div>
            `
                }

            }

            clearInterval(state.loop)
        }, 1000)
    }
}

//function to swap player turns
function currentTurn(player) {
    if (player) {
        player1Container.classList.add('current-turn');
        player2Container.classList.remove('current-turn');
    } else {
        player1Container.classList.remove('current-turn');
        player2Container.classList.add('current-turn');
    }
}

generateGame();
attachEventListeners();


//function used to restart the game from scratch
function restartGameFunc() {
    generateGame();
    attachEventListeners();
    clearInterval(state.loop); //reset board timer to stop it from increasing counting speed beyond oblivion when spamming Restart game btn
    startGame();

    //resets game stats to default
    state.totalTime = 0;
    state.score = 0;
    state.totalFlips = 0;
    p1Score = 0;
    p2Score = 0;
    p1Moves = 0;
    p2Moves = 0;
    state.flippedCards = [];

    //makes the game board and controls visible again
    selectors.boardContainer.classList.remove('flipped');
    selectors.controls.classList.remove('hidden');

    //makes sure the player names stay in the game when restarting
    selectors.player1Name.innerHTML = `${player1InputName.value}`;
    selectors.player2Name.innerHTML = `${player2InputName.value}`;
}
