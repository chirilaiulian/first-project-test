console.log('Verbing (ex10)');
console.log('   V         ');


function verbing(word) {
    if (word.length < 3) return word;

    let slicedWord3 = word.slice(-3);
    let slicedWord1 = word.slice(-1);

    if (slicedWord3 === 'ing') {
        return word + 'ly';
    } else if (slicedWord1 === 'm') {
        return word + 'ming';
    } else if (slicedWord1 === 'n') {
        return word + 'ning';
    } else {
        return word + 'ing';
    }
}

console.log(verbing('swim'));
console.log(verbing('swimming'));
console.log(verbing('go'));
console.log(verbing('run'));

console.log('');
