let books = [
    {
        title: 'The Design of Everyday Things',
        author: 'Don Norman',
        alreadyRead: false
    }, {
        title: 'The Most Human Human',
        author: 'Brian Christian',
        alreadyRead: true
    }
];

for (let i = 0; i < books.length; i++) {
    let bookParagraph = document.createElement('p');
    let bookDescription = document.createTextNode(books[i].title + ' by ' + books[i].author);
    bookParagraph.appendChild(bookDescription);
    document.body.appendChild(bookParagraph);
}