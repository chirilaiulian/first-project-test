//selecting all required elements
const boardCells = document.querySelectorAll('.board-cell');
const turn = document.querySelector('.turn');
const result = document.querySelector('.result');
const player1Name = document.getElementById('player1-name');
const player2Name = document.getElementById('player2-name');

// default player symbols
const playerOne = 'O';
const playerTwo = 'X';
// board array
var board = [
    ['', '', ''],
    ['', '', ''],
    ['', '', '']
];

// start the game

function startGame() {
    turn.innerHTML = `${player1.value}\'s turn`;
    boardCells.forEach((cell, index) => {
        cell.innerHTML = '';
        cell.addEventListener('click', handleClick.bind(null, cell, index));
    });
    player1Name.innerHTML = `${player1.value} plays with X`;
    player2Name.innerHTML = `${player2.value} plays with O`;
};

function handleClick(cell, index) {
    const cellValue = cell.innerHTML;
    if (cellValue === '') {
        if (turn.innerHTML === `${player1.value}\'s turn`) {
            cell.innerHTML = playerTwo;
            turn.innerHTML = `${player2.value}\'s turn`;
            // insert into array
            board[Math.floor(index / 3)][index % 3] = playerTwo; //figures out the correct cell to place each symbol
        } else {
            cell.innerHTML = playerOne;
            turn.innerHTML = `${player1.value}\'s turn`;
            // insert into array
            board[Math.floor(index / 3)][index % 3] = playerOne; //figures out the correct cell to place each symbol
        }
    }
    // remove event listener to make already filled cells unclickable
    cell.removeEventListener('click', handleClick);
    // check if someone won
    checkWinner();
}

function checkWinner() {
    // check for rows
    for (let i = 0; i < 3; i++) {
        if (board[i][0] === board[i][1] && board[i][0] === board[i][2] && board[i][0] !== '') {
            showResult(board[i][0]);
            return;
        }
    }
    // check for columns
    for (let i = 0; i < 3; i++) {
        if (board[0][i] === board[1][i] && board[0][i] === board[2][i] && board[0][i] !== '') {
            showResult(board[0][i]);
            return;
        }
    }
    // check for diagonals
    if (board[0][0] === board[1][1] && board[0][0] === board[2][2] && board[0][0] !== '') {
        showResult(board[0][0]);
        return;
    }
    if (board[0][2] === board[1][1] && board[0][2] === board[2][0] && board[0][2] !== '') {
        showResult(board[0][2]);
        return;
    }
    // check for a tie
    // if all cells are filled and no winner
    var count = 0;
    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            if (board[i][j] != '') {
                count++;
            }
        }
    }
    if (count == 9) {
        showResult('Tie');
        return;
    }
}

function showResult(symbol) {
    if (symbol === playerOne) {
        result.innerHTML = `<p>${player2.value} wins!</p>`;
    } else if (symbol === playerTwo) {
        result.innerHTML = `<p>${player1.value} wins!</p>`;
    } else {
        result.innerHTML = '<p>Draw!</p>';
    }
    multiPlayerGame.classList.add('hide-game');
    multiPlayerGame.classList.remove('show-game');
    result.style.display = 'flex';
    result.style.flexDirection = 'column-reverse';
    multiMainMenu.classList.remove('hide-menu');
    multiMainMenu.classList.add('show-menu');
}

function restartGame() {
    result.style.display = 'none';
    turn.innerHTML = `${player1.value}\'s turn`;

    board = [
        ['', '', ''],
        ['', '', ''],
        ['', '', '']
    ];

    startGame();
}