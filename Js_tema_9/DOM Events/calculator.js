function squareNumber(number) {
    let squareNumber = number ** 2;
    console.log(`The result of squaring ${number} is ${squareNumber}.`);
    return squareNumber;
}

function halfOf(number) {
    let half = number / 2;
    console.log(`Half of ${number} is ${half}.`);
    return half;
}

function percentOf(number1, number2) {
    let percent = ((number1 / number2) * 100).toFixed(2);
    console.log(`${number1} is ${percent}% of ${number2}.`);
    return percent + '%';
}

function areaOfCircle(radius) {
    let area = (Math.PI * squareNumber(radius)).toFixed(2);
    console.log(`The area of circle with radius ${radius} is ${area}.`);
    return area;
}


//square button action listener logic
let squareButton = document.getElementById('square-button');
function squareFunc() {
    let number = document.getElementById('square-input').value;
    document.getElementById('solution').innerHTML = squareNumber(number);
}
squareButton.addEventListener('click', squareFunc);


//half button action listener logic
let halfButton = document.getElementById('half-button');
function halfFunc() {
    let number = document.getElementById('half-input').value;
    document.getElementById('solution').innerHTML = halfOf(number);
}
halfButton.addEventListener('click', halfFunc);


//percent button action listener logic
let percentButton = document.getElementById('percent-button');
function percentFunc() {
    let number1 = document.getElementById('percent1-input').value;
    let number2 = document.getElementById('percent2-input').value;
    //not perfect but it works. (extra logic for bonus part of the homework)
    //removing NaN / Infinity% results when 1 of the numbers is undefined
    if (number1 && number2) {
        document.getElementById('solution').innerHTML = percentOf(number1, number2);
    }
}
percentButton.addEventListener('click', percentFunc);


//area of circle button action listener logic
let areaButton = document.getElementById('area-button');
function areaFunc() {
    let number = document.getElementById('area-input').value;
    document.getElementById('solution').innerHTML = areaOfCircle(number);
}
areaButton.addEventListener('click', areaFunc);



//Bonus: respond to key presses so that the user doesn't have to click the button
document.getElementById('square-input').addEventListener('keydown', squareFunc);

document.getElementById('half-input').addEventListener('keydown', halfFunc);

document.getElementById('percent1-input').addEventListener('keydown', percentFunc);
document.getElementById('percent2-input').addEventListener('keydown', percentFunc);

document.getElementById('area-input').addEventListener('keydown', areaFunc);