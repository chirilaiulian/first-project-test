console.log('The Fortune Teller (ex1)');
console.log('        V         ');

function tellFortune(nrOfChilren, partnerName, geoLoc, jobTitle) {
    console.log(`You will be a ${jobTitle} in ${geoLoc}, and married to ${partnerName} with ${nrOfChilren} kids.`)
}

tellFortune(3, 'Adrian', 'NYC', 'Dev');
tellFortune(4, 'Andrew', 'Bucharest', 'Librarian');
tellFortune(1, 'Miren', 'Iasi', 'Medic');

console.log('');