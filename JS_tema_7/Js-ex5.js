const celsiusTemp = 25;
let fahrenheitTemp = celsiusTemp * 1.8 + 32;
let fahrenheitConvertedToCelsius = (fahrenheitTemp - 32) * 5/9;

alert(`${celsiusTemp}°C is ${fahrenheitTemp}°F`);
alert(`${fahrenheitTemp}°F is ${fahrenheitConvertedToCelsius}°C`);