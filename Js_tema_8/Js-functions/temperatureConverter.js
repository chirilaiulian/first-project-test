console.log('The Temperature Converter (ex5)');
console.log('          V         ');


let celsius = 24;

function celsiusToFahrenheit(celsiusTemp) {
    let fahrenheitTemp = Math.round(celsiusTemp * 1.8 + 32);
    console.log(`${celsiusTemp}°C is ${fahrenheitTemp}°F`)
}

celsiusToFahrenheit(celsius);


let fahrenheit = 100;

function fahrenheitToCelsius(fahrenheitTemp) {
    let celsiusTemp = Math.round((fahrenheitTemp - 32) * (5 / 9));
    console.log(`${fahrenheitTemp}°F is ${celsiusTemp}°C`);
}

fahrenheitToCelsius(fahrenheit);

console.log('');