console.log('Dr Devil (ex7)');
console.log('   V         ');


function DrEvil(amount) {
    if (amount === 1000000) {
        return amount + " dollars (pinky)";
    } else {
        return amount + " dollars";
    }
}

console.log(DrEvil(10));
console.log(DrEvil(1000000));

console.log('');
