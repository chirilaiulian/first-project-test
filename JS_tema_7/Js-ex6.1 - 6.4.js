//6.1
const spaceShuttleName = 'Determination';
const spaceShuttleSpeed = 17500; //mph
const distanceToMars = 225000000; //km
const distanceToMoon = 384400; //km
const milesPerKilometer = 0.621;


//6.2
const milesToMars = distanceToMars * milesPerKilometer;
const hoursToMars = milesToMars / spaceShuttleSpeed;
const daysToMars = hoursToMars / 24;

//6.3
alert(`${spaceShuttleName} will take ${daysToMars} days to reach Mars.`);

//6.4
const milesToMoon = distanceToMoon * milesPerKilometer;
const hoursToMoon = milesToMoon / spaceShuttleSpeed;
const daysToMoon = hoursToMoon / 24;

alert(`${spaceShuttleName} will take ${daysToMoon} days to reach the Moon.`);