let catImgNode = document.getElementsByTagName('img')[0];
catImgNode.style.left = '0px';
let walkingForwards = true;

function catWalk() {
    //removed the bad stuttering out of the animation
    //that's why I set the cat to move by 1 pixel instead of 10 (10 times smoother)
    //catImgNode.style.left = (parseInt(catImgNode.style.left) + 1) + 'px';

    //bonus 1
    //When the cat reaches the right-hand of the screen, restart them at the left hand side ("0px"). 
    //So they should keep walking from left to right across the screen, forever and ever
    let currentPosition = parseInt(catImgNode.style.left);
    // if (currentPosition > (window.innerWidth - catImgNode.width)) {
    //     catImgNode.style.left = '0px';
    // }

    //bonus 2
    //When the cat reaches the right-hand of the screen, restart them at the left hand side ("0px").
    //So they should keep walking from left to right across the screen, forever and ever.
    if (walkingForwards && (currentPosition > (window.innerWidth - catImgNode.width))) {
        walkingForwards = false;
    }

    //stops the cat from leaving the left side of the screen
    if (!walkingForwards && (currentPosition <= 0)) {
        walkingForwards = true;
    }

    if (walkingForwards) {
        catImgNode.style.left = (currentPosition + 1) + 'px';
        catImgNode.style.transform = 'scaleX(1)';
    } else {
        catImgNode.style.left = (currentPosition - 1) + 'px';
        catImgNode.style.transform = 'scaleX(-1)';
    }
}

window.setInterval(catWalk, 1); //I move the cat every 1 ms to make it move faster
