function makeMadLib() {
    let story = document.getElementById('story');
    let person = document.getElementById('person').value;
    let adjective = document.getElementById('adjective').value;
    let noun = document.getElementById('noun').value;
    story.innerHTML = person + ' really likes ' + adjective + ' ' + noun + '.';
}

let button = document.getElementById('lib-button');
button.addEventListener('click', makeMadLib);