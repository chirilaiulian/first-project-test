console.log('The Calculator (ex6)');
console.log('       V         ');


function squareNumber(number) {
    let squared = number ** 2;
    console.log(`The result of squaring the number ${number} is ${squared}.`);
    return squared;
}

squareNumber(4);

function halfNumber(number) {
    let half = (number / 2).toFixed(2);
    console.log(`Half of ${number} is ${half}.`);
    return half;
}

halfNumber(223);

function percentOf(number1, number2) {
    let percent = ((number1 / number2) * 100).toFixed(1);
    console.log(`${number1} is ${percent}% of ${number2}.`);
    return percent;
}

percentOf(24, 77);

function areaOfCircle(radius) {
    let area = (Math.PI * radius ** 2).toFixed(2);
    console.log(`The area of a circle with a radius of ${radius} is ${area}.`);
    return area;
}

areaOfCircle(25);

function theFunctionator(number) {
    let half = halfNumber(number);
    let square = squareNumber(half);
    let area = areaOfCircle(square);
    let percent = percentOf(area, square);

    console.log(`What an amazing result :D ${percent}.`);
}

theFunctionator(10);

console.log('');