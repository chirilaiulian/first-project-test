//selecting all required elements
let singlePlayerBtn = document.getElementsByClassName('single')[0];
let multiplayerBtn = document.getElementsByClassName('multi')[0];
let mainMenu = document.getElementsByClassName('main-menu-cover')[0];
let singlePlayerGame = document.getElementsByClassName('select-box')[0];
let multiPlayerGame = document.getElementsByClassName('tic-tac-toe')[0];
let backToMainMenuSingle = document.getElementsByClassName('menu-single')[0];
let backToMainMenuMulti = document.getElementsByClassName('menu-multi')[0];
let options = document.getElementsByClassName('options')[0];
let multiMainMenu = document.getElementsByClassName('multi-main-menu')[0];

let player1 = document.getElementById('player1');//get player names
let player2 = document.getElementById('player2');
let startMultiGame = document.getElementsByClassName('start-multi-game')[0];

let active = false; //boolean value to make the options togglable

singlePlayerBtn.onclick = () => { //black magic to make menus dissapear and games appear c:
    mainMenu.classList.remove('show-menu');
    mainMenu.classList.add('hide-menu');
    singlePlayerGame.classList.remove('hide-game');
    singlePlayerGame.classList.add('show-game');
}

multiplayerBtn.onclick = () => { //togglable menu logic
    if (active) {
        options.classList.add('hide-menu');
        options.classList.remove('show-menu');
        active = !active;
    } else {
        active = !active;
        options.classList.remove('hide-menu');
        options.classList.add('show-menu');
    }

}

backToMainMenuSingle.onclick = () => { //logic to make the back to menu button work
    mainMenu.classList.remove('hide-menu');
    mainMenu.classList.add('show-menu');
    singlePlayerGame.classList.remove('show-game');
    singlePlayerGame.classList.add('hide-game');
}

function backToMenu() { //this button caused huge pain to debug
    window.location.reload();
}

//that's why this is commented

// backToMainMenuMulti.onclick = () => {
//     mainMenu.classList.remove('hide-menu');
//     mainMenu.classList.add('show-menu');
//     multiPlayerGame.classList.remove('show-game');
//     multiPlayerGame.classList.add('hide-game');
// }

startMultiGame.onclick = () => {  //input field validation for multiplayer names
    if (player1.value === "" || player2.value === "") {
        document.getElementsByClassName('validation')[0].innerHTML = '<p>Don\'t forget to enter player names c:</p>';
    } else {
        mainMenu.classList.remove('show-menu');
        mainMenu.classList.add('hide-menu');
        multiPlayerGame.classList.remove('hide-game');
        multiPlayerGame.classList.add('show-game');
    }

    if (player1.value === "") {
        player1.setAttribute("placeholder", "Player 1 is still missing :c");
        player1.classList.add('incorrect-field');
    }
    if (player1.value === "") {
        player2.setAttribute("placeholder", "Player 2 is still missing :c");
        player2.classList.add('incorrect-field');
    }
    startGame(); //multiplayer game function called from another script
    //this makes sure the game starts with non empty name values
}

player1.onkeydown = () => {
    player1.classList.remove('incorrect-field');
}

player2.onkeydown = () => {
    player2.classList.remove('incorrect-field');
}