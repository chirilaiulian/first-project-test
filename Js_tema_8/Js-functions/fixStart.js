console.log('Fix Start (ex9)');
console.log('     V         ');

function fixStart(string) {
    let character = string.charAt(0);
    let result = character + string.slice(1).replace(new RegExp(character, 'g'), '*');
    return result;
}

console.log(fixStart('babble'));

console.log('');